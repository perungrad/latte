<?php

use Latte\Engine;
use Nette\Utils\Json;
use Tracy\Debugger;
use WorkshopLatte\LatteFactory;

require __DIR__ . '/../vendor/autoload.php';

Debugger::enable();

$rootDir = realpath(__DIR__ . '/..');

$data = file_get_contents($rootDir . '/data/data.json');

$factory = new LatteFactory($rootDir . '/var/cache/templates');

$template = $factory->create();

$template->render($rootDir . '/templates/homepage.latte', [
    'data' => Json::decode($data),
]);
