var gulp = require('gulp');

gulp.task('copy', function () {
    gulp
        .src([
            'assets/vendor/**/*',
        ])
        .pipe(gulp.dest('www/assets/vendor/'));
});

gulp.task('default', ['copy']);
