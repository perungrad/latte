<?php

namespace WorkshopLatte;

use Latte\Engine;

class LatteFactory
{
    /** @var string */
    private $tempDirectory;

    /**
     * @param string $tempDirectory
     */
    public function __construct($tempDirectory)
    {
        $this->tempDirectory = $tempDirectory;
    }

    /**
     * @return Engine
     */
    public function create()
    {
        $template = new Engine();

        $template->setTempDirectory($this->tempDirectory);

        $template->addFilter('randomize', function ($str) {
            $chars = preg_split('//u', $str, -1, PREG_SPLIT_NO_EMPTY);

            shuffle($chars);

            return join('', $chars);
        });

        $template->addFilter('shorten', function ($str, $len = 10) {
            return mb_substr($str, 0, $len);
        });

        return $template;
    }
}

