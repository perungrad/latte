<?php

namespace WorkshopLatte;

class BuildingTypes
{
    const LUMBERJACK   = 1;
    const QUARRY       = 2;
    const IRON_MINE    = 3;
    const FARMS        = 4;
    const STOREHOUSE   = 5;
    const MARKET       = 6;
    const BARRACKS     = 7;
    const ARCHERS_YARD = 8;
    const STABLES      = 9;
    const WORKSHOP     = 10;
    const HIDEOUT      = 11;
    const CASTLE       = 12;
    const WALL         = 13;

    protected static $stringIds = [
        self::LUMBERJACK   => 'lumberjack',
        self::QUARRY       => 'quarry',
        self::IRON_MINE    => 'iron-mine',
        self::FARMS        => 'farms',
        self::STOREHOUSE   => 'storehouse',
        self::MARKET       => 'market',
        self::BARRACKS     => 'barracks',
        self::ARCHERS_YARD => 'archers-yard',
        self::STABLES      => 'stables',
        self::WORKSHOP     => 'workshop',
        self::HIDEOUT      => 'hideout',
        self::CASTLE       => 'castle',
        self::WALL         => 'wall',
    ];

    /**
     * @param integer $typeId
     *
     * @return string
     */
    public static function getStringId($typeId)
    {
        if (array_key_exists($typeId, self::$stringIds)) {
            return self::$stringIds[$typeId];
        }

        throw new \Exception('Oh crap!');
    }
}

